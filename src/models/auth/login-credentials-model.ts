
// Clase contenedora para las credenciales del login.
export class LoginCredentialsModel {
  nickName: string;
  password: string;


  constructor(nickName: string, password: string) {
    this .nickName = nickName;
    this .password = password; 
  }
}
