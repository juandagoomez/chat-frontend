export const environment = {
   //serverUrl: 'http://localhost:4000/',
   serverUrl: 'https://kuepa.herokuapp.com/',
   production: 'https://kuepa.herokuapp.com/',
   socketUrl:"https://kuepa.herokuapp.com"
};
