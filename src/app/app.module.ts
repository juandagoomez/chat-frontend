import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VexModule } from '../@vex/vex.module';
import { HttpClientModule } from '@angular/common/http';
import { CustomLayoutModule } from './custom-layout/custom-layout.module';
import { LoginModule } from './pages/auth/login/login.module';
import { RoleAuthGuard } from './shared/auth/roleAuth-guard.service';
import { UserCrudModule } from './pages/user-crud/user-crud.module';
import { ChatModule } from './pages/chat/chat.module';
import { ChatService } from './shared/chat/chat.service';
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    LoginModule,
    ChatModule,
    UserCrudModule,   

    // Vex
    VexModule,
    CustomLayoutModule
  ],
  providers: [
    RoleAuthGuard,
    ChatService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
