import { Router } from '@angular/router';
// Tener en cuenta en esta clase las rutas cuando el token no está expirado, esta clase
// permitirá además el enrutamiento entre las páginas cuando el usuario este logueado.

import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { TokenStorageService } from '../storage-services/token-storage.service';

@Injectable()
export class RoleAuthGuard implements CanActivate {

  constructor(
    private router: Router,
   
    private tokenStorage: TokenStorageService
  ) { }

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    const expectedRole: string[] = await route.data.expectedRole;
    for (const i in expectedRole) {
      if (this.tokenStorage.getToken() != null) {
        if (this.tokenStorage.getAuthorities() === expectedRole[i]) {
          return true;
        }
      } else {
        this.router.navigate(['login']);
        return false;
      }
    }
    this.router.navigate(['login']);
    return false;
  }
}
