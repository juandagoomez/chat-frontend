import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { trackById } from '../../../../@vex/utils/track-by';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icSend from '@iconify/icons-ic/twotone-send';
import icCheckCircle from '@iconify/icons-ic/twotone-check-circle';
import icAccountCircle from '@iconify/icons-ic/twotone-account-circle';
import icClearAll from '@iconify/icons-ic/twotone-clear-all';
import icVolumeMute from '@iconify/icons-ic/twotone-volume-mute';
import { fadeInUp400ms, } from '../../../../@vex/animations/fade-in-up.animation';
import { FormControl, FormGroup } from '@angular/forms';
import { stagger20ms } from '../../../../@vex/animations/stagger.animation';
import { ScrollbarComponent } from '../../../../@vex/components/scrollbar/scrollbar.component';
import { ChatService } from 'src/app/shared/chat/chat.service';
import { NgScrollbar } from 'ngx-scrollbar';
import { TokenStorageService } from 'src/app/shared/storage-services/token-storage.service';
import { UserModel } from 'src/models/entities/user.model';
import { CrudServiceService } from 'src/app/shared/api/crud-service.service';
import { InfoMessagesService } from 'src/app/shared/messages/info-messages.service';

@Component({
  selector: 'vex-chat-conversation',
  templateUrl: './chat-conversation.component.html',
  styleUrls: ['./chat-conversation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    fadeInUp400ms,
    stagger20ms
  ]
})
export class ChatConversationComponent implements OnInit, OnDestroy {

  messages: any;

  form = new FormGroup({
    message: new FormControl()
  });

  idUser = this.tokenStorage.getId();

  icAccountCircle = icAccountCircle;
  icClearAll = icClearAll;
  icVolumeMute = icVolumeMute;
  icCheckCircle = icCheckCircle;
  icMoreVert = icMoreVert;
  icSend = icSend;
  trackById = trackById;
  @ViewChild(NgScrollbar) scrollbarRef: NgScrollbar;

  @ViewChild(ScrollbarComponent, { static: true }) scrollbar: ScrollbarComponent;

  constructor(private cd: ChangeDetectorRef,
    private chatService: ChatService,
    private tokenStorage: TokenStorageService,
    private apiService: CrudServiceService, private infoMessages: InfoMessagesService) { }

  ngOnInit() {
    this.messages = [];
    this.getChats();
  }


  listenChats() {
    this.chatService
      .getMessages()
      .subscribe((message: any) => {
        this.messages.push(message);
        console.log(message);
        this.cd.detectChanges();
      });
  }



  getChats() {
    let path = 'chats'
    this.apiService.getModel(path).subscribe(result => {
      console.log(result);
      this.messages = result;
      this.cd.detectChanges();
      this.listenChats();
    },
      error => {
        this.infoMessages.getInfoMessageError();

      })
  }

  send() {
    let sender = new UserModel(this.idUser, this.tokenStorage.getUsername(), this.tokenStorage.getTypeUser())
    let chat = {
      message: this.form.get('message').value,
      sender: sender,
      created: new Date()
    };
    this.messages.push(chat);
    this.chatService.sendMessage(chat);
    this.form.get('message').setValue('');

    this.cd.detectChanges();
    //this.scrollToBottom();

  }

  scrollToBottom() {
    this.scrollbar.scrollbarRef.getScrollElement().scrollTo({
      behavior: 'smooth',
      top: this.scrollbar.scrollbarRef.getContentElement().clientHeight
    });
  }

  ngOnDestroy(): void { }
}
