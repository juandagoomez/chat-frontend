import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import icSearch from '@iconify/icons-ic/twotone-search';
import icChat from '@iconify/icons-ic/twotone-chat';
import { fadeInUp400ms } from '../../../@vex/animations/fade-in-up.animation';
import { trackById } from '../../../@vex/utils/track-by';
import { stagger80ms } from '../../../@vex/animations/stagger.animation';
import icCheckCircle from '@iconify/icons-ic/twotone-check-circle';



@Component({
  selector: 'vex-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    fadeInUp400ms,
    stagger80ms
  ]
})
export class ChatComponent implements OnInit, OnDestroy {


  mobileQuery = this.mediaMatcher.matchMedia('(max-width: 1279px)');


  icCheckCircle = icCheckCircle;
  icSearch = icSearch;
  icChat = icChat;
  trackById = trackById;
  private _mobileQueryListener: () => void;

  constructor(private cd: ChangeDetectorRef,
    private mediaMatcher: MediaMatcher) { }

  ngOnInit() {
    this._mobileQueryListener = () => this.cd.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
  }



  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
  }
}
